CREATE DATABASE IF NOT EXISTS librart;
USE librart;

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS author;
DROP TABLE IF EXISTS oeuvre;
DROP TABLE IF EXISTS post;



CREATE TABLE IF NOT EXISTS user(  
    userId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    email VARCHAR(255) UNIQUE KEY NOT NULL,
    userPassword VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS author (  
    authorId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL,
    birthDate DATE NOT NULL,
    birthPlace VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS oeuvre (  
    oeuvreId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    descri TEXT NOT NULL,
    releasedDate DATE NOT NULL,
    userId INTEGER NOT NULL,
    authorId INTEGER NOT NULL,

    FOREIGN KEY (userId) REFERENCES user(userId),
    FOREIGN KEY (authorId) REFERENCES author(authorId)
);

CREATE TABLE post (  
    postId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    postDate TIMESTAMP NOT NULL,
    postMessage TEXT NOT NULL,
    likes INTEGER,
    userId INTEGER NOT NULL,
    oeuvreId INTEGER NOT NULL,

    FOREIGN KEY (userId) REFERENCES user(userId),
    FOREIGN KEY (oeuvreId) REFERENCES oeuvre(oeuvreId)
);

INSERT INTO user (firstName, lastName, email, userPassword) 
VALUES ('First Name', 'Last Name', 'untel@gmail.com', 'password');

INSERT INTO author (firstName, lastName, birthDate) 
VALUES ('Claude', 'Monet', '1840-11-14');

INSERT INTO oeuvre (title, descri, releasedDate, userId, authorId)
VALUES ('Impresssion, Soleil Levant', "Impression, soleil levant est un tableau de Claude Monet conservé au musée Marmottan à Paris, dont le titre donné pour la première exposition impressionniste d'avril 1874 a donné son nom au courant de l'impressionnisme.",'1872-11-13', '1', '1');

INSERT INTO post (postMessage, likes, userId, oeuvreId)
VALUES ("Love this !", '1', '1', '1');