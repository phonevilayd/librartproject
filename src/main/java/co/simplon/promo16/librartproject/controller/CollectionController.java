package co.simplon.promo16.librartproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CollectionController {

    @GetMapping("/collection")
    public String showCollection() {
        return "collection";
    }
    
}
