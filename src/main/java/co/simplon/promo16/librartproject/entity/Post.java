package co.simplon.promo16.librartproject.entity;

import java.sql.Timestamp;

public class Post {
    private Integer postId;
    private Timestamp postDate;
    private String postMessage;
    private boolean likes;
    
    public Post(Timestamp postDate, String postMessage, boolean likes) {
        this.postDate = postDate;
        this.postMessage = postMessage;
        this.likes = likes;
    }

    public Post(Integer postId, Timestamp postDate, String postMessage, boolean likes) {
        this.postId = postId;
        this.postDate = postDate;
        this.postMessage = postMessage;
        this.likes = likes;
    }

    public Post() {
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Timestamp getPostDate() {
        return postDate;
    }

    public void setPostDate(Timestamp postDate) {
        this.postDate = postDate;
    }

    public String getPostMessage() {
        return postMessage;
    }

    public void setPostMessage(String postMessage) {
        this.postMessage = postMessage;
    }

    public boolean isLikes() {
        return likes;
    }

    public void setLikes(boolean likes) {
        this.likes = likes;
    }

    
    
   

    
   
}
