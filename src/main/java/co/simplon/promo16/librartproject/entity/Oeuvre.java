package co.simplon.promo16.librartproject.entity;

import java.sql.Date;

public class Oeuvre {
    private Integer oeuvreId;
    private String title;
    private Date relasedDate;
    private String descri;

    
    public Oeuvre() {
    }


    public Oeuvre(String title, Date relasedDate, String descri) {
        this.title = title;
        this.relasedDate = relasedDate;
        this.descri = descri;
    }


    public Oeuvre(Integer oeuvreId, String title, Date relasedDate, String descri) {
        this.oeuvreId = oeuvreId;
        this.title = title;
        this.relasedDate = relasedDate;
        this.descri = descri;
    }


    public Integer getOeuvreId() {
        return oeuvreId;
    }


    public void setOeuvreId(Integer oeuvreId) {
        this.oeuvreId = oeuvreId;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public Date getRelasedDate() {
        return relasedDate;
    }


    public void setRelasedDate(Date relasedDate) {
        this.relasedDate = relasedDate;
    }


    public String getDescri() {
        return descri;
    }


    public void setDescri(String descri) {
        this.descri = descri;
    }

    
    


    
}