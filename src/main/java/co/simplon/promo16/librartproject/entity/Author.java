package co.simplon.promo16.librartproject.entity;

import java.time.LocalDate;

public class Author {
    private Integer authorId;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String birthPlace;
   
   
    public Author(String firstName, String lastName, LocalDate birthDate, String birthPlace) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.birthPlace = birthPlace;
    }


    public Author(Integer authorId, String firstName, String lastName, LocalDate birthDate, String birthPlace) {
        this.authorId = authorId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.birthPlace = birthPlace;
    }


    public Author() {
    }


    public Integer getAuthorId() {
        return authorId;
    }


    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public LocalDate getBirthDate() {
        return birthDate;
    }


    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }


    public String getBirthPlace() {
        return birthPlace;
    }


    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }
    
}
