package co.simplon.promo16.librartproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class LibrartprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibrartprojectApplication.class, args);
	}

}
