package co.simplon.promo16.librartproject.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import co.simplon.promo16.librartproject.entity.Author;

@Repository
public class AuthorRepository implements IRepository<Author>{

    private Connection connection;

    public AuthorRepository() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/librart");
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    @Override
    public List<Author> findAll() {
        List<Author> authorList = new ArrayList<>();
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM author");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Author author = new Author(
                    result.getInt("authorId"),
                    result.getString("firstName"),
                    result.getString("lastName"),
                    result.getDate("birthDate").toLocalDate(),
                    result.getString("birthPlace"));
                
                authorList.add(author);
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Author findById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT FROM author WHERE id=?");
            ResultSet result = stmt.executeQuery();
            result.next();

            if(result.getInt("id")== id) {
                Author author = new Author(
                    result.getInt("authorId"),
                    result.getString("firstName"),
                    result.getString("lastName"),
                    result.getDate("birthdate").toLocalDate(),
                    result.getString("birthPlace"));
                
                    return author;
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean save(Author entity) {
        try {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO author(firstName, lastName, birthDate, birthPlace) VALUES(?)", PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, entity.getFirstName());
            stmt.setString(2, entity.getLastName());
            stmt.setDate(3, Date.valueOf(entity.getBirthDate()));
            stmt.setString(4, entity.getBirthPlace());

            if(stmt.executeUpdate() == 1){
                // ResultSet result = stmt.executeQuery();
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                entity.setAuthorId(result.getInt(1));

                return true;

            }
            
            
        } catch (SQLException e) {
        
            e.printStackTrace();
        }

        return false;
        
    }

    @Override
    public void update(Author entity) {
        try {
            PreparedStatement stmt = connection.prepareStatement("UPDATE author SET firstName=?, lastName=?, birthDate=?, birthPlace=? WHERE id=?");
            stmt.setString(1, entity.getFirstName());
            stmt.setString(2, entity.getLastName());
            stmt.setDate(3, Date.valueOf(entity.getBirthDate()));
            stmt.setInt(4, entity.getAuthorId());

           
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        
    }

    @Override
    public void deleteById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM author WHERE id=?");
            stmt.setInt(1, id);
        } catch (SQLException e) {
           
            e.printStackTrace();
        }
        
    }
    
}
