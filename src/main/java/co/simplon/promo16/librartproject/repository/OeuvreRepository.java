package co.simplon.promo16.librartproject.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import co.simplon.promo16.librartproject.entity.Oeuvre;

@Repository
public class OeuvreRepository implements IRepository<Oeuvre> {

    private Connection connection;

    public OeuvreRepository() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/librart");
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    @Override
    public List<Oeuvre> findAll() {
        List<Oeuvre> oeuvreList = new ArrayList<>();
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM oeuvre");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Oeuvre oeuvre = new Oeuvre(
                        result.getInt("oeuvreId"),
                        result.getString("title"),
                        result.getDate("releasedDate"),
                        result.getString("descri"));

                oeuvreList.add(oeuvre);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Oeuvre findById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT FROM oeuvre WHERE id=?");
            ResultSet result = stmt.executeQuery();
            result.next();

            if (result.getInt("id") == id) {
                Oeuvre oeuvre = new Oeuvre(
                        result.getInt("oeuvreId"),
                        result.getString("title"),
                        result.getDate("releasedDate"),
                        result.getString("descri"));

                return oeuvre;
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean save(Oeuvre entity) {
        try {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO oeuvre(title, releasedDate, descri) VALUES(?)", PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, entity.getTitle());
            stmt.setDate(2, entity.getRelasedDate());
            stmt.setString(3, entity.getDescri());
        

            if(stmt.executeUpdate() == 1){
                // ResultSet result = stmt.executeQuery();
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                entity.getOeuvreId();

                return true;

            }
            
            
        } catch (SQLException e) {
        
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void update(Oeuvre entity) {
        try {
            PreparedStatement stmt = connection.prepareStatement("UPDATE oeuvre SET title=?, releasedDate=?, descri=? WHERE id=?");
            stmt.setString(1, entity.getTitle());
            stmt.setDate(2, entity.getRelasedDate());
            stmt.setString(3, entity.getDescri());

           
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        

    }

    @Override
    public void deleteById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM oeuvre WHERE id=?");
            stmt.setInt(1, id);
        } catch (SQLException e) {
           
            e.printStackTrace();
        }

    }

}
