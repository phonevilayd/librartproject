package co.simplon.promo16.librartproject.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import co.simplon.promo16.librartproject.entity.Post;

@Repository
public class PostRepository implements IRepository<Post> {

    private Connection connection;

    public PostRepository() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/librart");
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    @Override
    public List<Post> findAll() {
        List<Post> postList = new ArrayList<>();
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM post");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Post post = new Post(
                    result.getInt("postId"),
                    result.getTimestamp("postDate"),
                    result.getString("postMessage"),
                    result.getBoolean("likes"));
                
                postList.add(post);
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Post findById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT FROM post WHERE id=?");
            ResultSet result = stmt.executeQuery();
            result.next();

            if(result.getInt("id")== id) {
                Post post = new Post(
                    result.getInt("postId"),
                    result.getTimestamp("postDate"),
                    result.getString("postMessage"),
                    result.getBoolean("likes"));
                
                    return post;
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean save(Post entity) {
        try {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO post(postId, postDate, postMessage, likes) VALUES(?)", PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, entity.getPostId());
            stmt.setTimestamp(2, entity.getPostDate());
            stmt.setString(3, entity.getPostMessage());
            stmt.setBoolean(4, entity.isLikes());

            if(stmt.executeUpdate() == 1){
                // ResultSet result = stmt.executeQuery();
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                entity.setPostId(result.getInt(1));

                return true;

            }
            
            
        } catch (SQLException e) {
        
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void update(Post entity) {
        try {
            PreparedStatement stmt = connection.prepareStatement("UPDATE post SET postId=?, postDate=?, postMessage=?, likes=? WHERE id=?");
            stmt.setInt(1, entity.getPostId());
            stmt.setTimestamp(2, entity.getPostDate());
            stmt.setString(3, entity.getPostMessage());
            stmt.setBoolean(4, entity.isLikes());
           
        } catch (SQLException e) {
            
            e.printStackTrace();
        }

    }

    @Override
    public void deleteById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM post WHERE id=?");
            stmt.setInt(1, id);
        } catch (SQLException e) {
           
            e.printStackTrace();
        }

    }

}
